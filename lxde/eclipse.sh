#!/bin/bash
#
# CMD script inside the container
#
# Based on https://github.com/fgrehm/docker-eclipse/blob/master/run
#
# Markus Juenemann, 23-Oct-2015

# Make sure the user data directory is owned by the developer user
#
if [ -d /home/developer/.eclipse ]; then
  sudo chown developer:developer /home/developer/.eclipse
fi
exec /opt/eclipse/eclipse
