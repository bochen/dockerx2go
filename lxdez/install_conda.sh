#!/usr/bin/bash

set -xe


wget --quiet https://repo.continuum.io/miniconda/Miniconda2-4.4.10-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p ~/miniconda2 && \
    rm ~/miniconda.sh 

echo 'export PATH=$HOME/miniconda2/bin:$PATH' >> ~/.bashrc

export PATH=$HOME/miniconda2/bin:$PATH



conda update -n base conda && conda config --add channels conda-forge --force

conda install -c conda-forge nb_conda

conda create -c conda-forge -n python27 python=2.7 fastparquet python-snappy geopandas dask ipykernel feather-format fbprophet tensorflow=1.2.1 scikit-learn joblib statsmodels








