#!/usr/bin/bash

set -xe

mkdir /tmp/build
cd /tmp/build

wget https://cran.r-project.org/src/base/R-3/R-3.4.2.tar.gz

tar xf R-3.4.2.tar.gz
cd R-3.4.2
./configure --with-lapack=/opt/OpenBLAS/lib/libopenblas.so --with-blas=/opt/OpenBLAS/lib/libopenblas.so --with-readline=no --with-x=no --prefix /opt/R_OpenBLAS

make -j8 
make install

cd /
rm -rf /tmp/build
rm -rf /build_openblas.sh
