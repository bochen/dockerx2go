#!/usr/bin/bash

set -xe

ln -s /opt/R_OpenBLAS/bin/* /usr/bin/

/opt/R_OpenBLAS/bin/Rscript -e 'install.packages(c("optparse"), repos="http://mirrors.nics.utk.edu/cran/")'
/opt/R_OpenBLAS/bin/Rscript -e 'install.packages(c("feather"), repos="http://mirrors.nics.utk.edu/cran/")'
/opt/R_OpenBLAS/bin/Rscript -e 'install.packages(c("reticulate"), repos="http://mirrors.nics.utk.edu/cran/")'
/opt/R_OpenBLAS/bin/Rscript -e 'install.packages(c("forecast"), repos="http://mirrors.nics.utk.edu/cran/")'
/opt/R_OpenBLAS/bin/Rscript -e 'install.packages(c("zoo"), repos="http://mirrors.nics.utk.edu/cran/")'





